#ifndef PROJET_H
# define PROJET_H

# include "../libft/includes/libft.h"
# include "../minilibx/mlx.h"
# include "../minilibx/mlx_int.h"
# include <mlx.h>
# include <X11/Xlib.h>
# include <X11/Xutil.h>
# include "/opt/X11/include/X11/Xlib.h"
# include <mlx.h>
# include <math.h>

# define LOG_PATH	"logs/debugg.log"
# define MAXSCENAR	3
# define JULIA		1
# define MANDELBROT	2
# define NEWTON	3

# define WINX		400
# define WINY		400

# define MITER 	50

# define JITER		50
# define JREAL		-0.7
# define JIMAGINARY	0.27015
# define JMOVE 	0.0002

# define NITER		50
# define NXBEGIN	-2.0
# define NXEND	2.0
# define NYBEGIN	-2.0
# define NYEND	2.0

# define TITLE		"Fract'ol by Jean-Jacques MOIROUX"

typedef struct 			s_conf
{
	void			*mlx;
	void			*win;
}				t_conf;

typedef struct 			s_color
{
	int 			red;
	int 			green;
	int 			blue;
}				t_color;

typedef struct 			s_julia
{
	double			new_real;
	double			new_imaginary;
	double			old_real;
	double			old_imaginary;
	int			x;
	int			y;
	int			i;
	int			run;
}				t_julia;

typedef struct 			s_mandelbrot
{
	double			x;
	double			xx;
	double			y;
	double			cx;
	double			cy;
	double			index_x;
	double			index_y;
	double			color_mult;
	int			i;
	int			run;
}				t_mandelbrot;

typedef struct 			s_newton
{
	double			index_x;
	double			index_y;
	double			x;
	double			y;
	double			xx;
	double			yy;
	double			n1;
	double			n2;
	int			i;
}				t_newton;

typedef struct			s_data
{
	int			fd_log;
	int			i_argv;
	short int		scenar;
	unsigned short int	op_logs;
	unsigned short int 	op_mmotion;
	unsigned short int 	show_help;
	unsigned short int 	op_win;
	// render minilibx
	t_conf			*render;
	t_color			*color;
	void			*img_new;
	char			*img_data;
	int 			bit_pixel;
	int 			line_size;
	int 			endian;
	int 			winx;
	int 			winy;
	// julia
	t_julia			*j;
	double			j_moveX;
	double			j_moveY;
	double			j_zoom;
	int 			j_iter_max;
	double			c_real;
	double			c_imaginary;
	//mandelbrot
	t_mandelbrot		*m;
	double			m_zoom;
	int 			m_iter_max;
	// newton
	t_newton		*n;
	int 			n_iter_max;
	double			n_deltax;
	double			n_deltay;
}				t_data;

int				error(char const *str);
int				init_start(t_data **d, int ac, char **av);
int				open_log(t_data *d);
int				scenar_alloc(t_data *d);
int 				check_option(t_data *d, int ac, char **av);
t_data				*getData(void);
void				extra_infos(t_data *d);
void				op_mmotion(t_data *d);
void				op_win(t_data *d, int ac, char **av);
void				print_commands(void);
void				w_log(char const *str);
void 				op_logs(t_data *d);

int				expose_hook(t_data *d);
int				key_hook(int keycode, t_data *d);
int				mouse_hook(int button, int x, int y, t_data *d);
int				pointer_hook(int x, int y, t_data *d);
int				render_init(t_data *d);
int				render_scene(t_data *d);
void				render_set_color(int red, int green, int blue);
void				run_fractol(t_data *d);

int				mlx_pixel_image(int x, int y, t_data *d);
int				mlx_pointer_hook(t_win_list *win_ptr,
					int (*funct_ptr)(), void *param);

void				run_julia(t_data *d);
void				run_manderlbrot(t_data *d);
void				run_newton(t_data *d);

int				key_echap(t_data *d);
int				key_m(t_data *d);
int				key_l(t_data *d);
int				key_add(t_data *d);
int				key_less(t_data *d);
int				key_left(t_data *d);
int				key_right(t_data *d);
int				key_up(t_data *d);
int				key_down(t_data *d);
int				key_a(t_data *d);
int				key_d(t_data *d);
int				key_w(t_data *d);
int				key_s(t_data *d);
int				key_r(t_data *d);
int				key_1(t_data *d);
int				key_2(t_data *d);
int				key_3(t_data *d);
int				mouse_wheel_down(t_data *d, int x, int y);
int				mouse_wheel_up(t_data *d, int x, int y);

#endif
