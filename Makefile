# change with $(GCC) or $(CLANG) depend system
CC = $(CLANG)

# change your includes
HEADER = includes/projet.h

# change binary name
NAME = fractol

# add sources files
SRCS =	src/main.c \
	src/basic_functions.c \
	src/init_start.c \
	src/check_option.c \
	src/op_logs.c \
	src/op_mmotion.c \
	src/op_win.c \
	src/extra_infos.c \
	src/render_init.c \
	src/render_expose_hook.c \
	src/render_key_bind.c \
	src/render_key_bind_1.c \
	src/render_key_bind_2.c \
	src/render_key_bind_3.c \
	src/render_key_bind_4.c \
	src/render_scene.c \
	src/render_run_fractol.c \
	src/render_mouse_hook.c \
	src/render_pointer_hook.c \
	src/render_set_color.c \
	src/mlx_pointer_hook.c \
	src/mlx_pixel_to_image.c \
	src/mouse_wheel_down.c \
	src/mouse_wheel_up.c \
	src/scenar_allocation.c \
	src/scenar_julia.c \
	src/scenar_mandelbrot.c \
	src/scenar_newton.c

# Don'y modify following
GCC = gcc
CLANG = clang
CFLAGS = -Wall -Werror -Wextra -pedantic -o3 -w
LIB = libft/libft.a
MLX = minilibx/libmlx.a
OBJS = $(SRCS:.c=.o)

all: $(NAME)

$(LIB):
	make -C libft

$(MLX):
	make -C minilibx

%.o: %.c $(HEADER)
	@$(CC) $(CFLAGS) -I/libft -I/usr/X11R6/include -I/usr/X11/lib -c $< -o $@

$(NAME): $(LIB) $(MLX) $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $(NAME) \
	-L libft -lft \
	-L minilibx -lmlx \
	-L/usr/X11/lib -lXext -lX11

clean:
	make clean -C libft
	make clean -C minilibx
	rm -rf $(OBJS)

fclean: clean
	make fclean -C libft
	make fclean -C minilibx
	rm -rf $(NAME)

re: fclean all

.PHONY: all clean fclean re
