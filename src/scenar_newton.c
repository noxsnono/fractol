#include "../includes/projet.h"

static void	run_newton_1(t_data *d)
{
	d->n->x = d->n->index_x * d->n_deltax + NXBEGIN;
	d->n->y = d->n->index_y * d->n_deltay + NYBEGIN;
	d->n->i = 0;
	while (d->n->i < d->n_iter_max)
	{
		d->n->xx = d->n->x * d->n->x;
		d->n->yy = d->n->y * d->n->y;
		d->n->n1 = 3.0 * ((d->n->xx - d->n->yy)
			* (d->n->xx - d->n->yy) + 4.0 * d->n->xx * d->n->yy);
		if (d->n->n1 == 0.0)
			d->n->n1 = 0.000001;
		d->n->n2 = d->n->x;
		d->n->x = (2.0 / 3.0) * d->n->x + (d->n->xx - d->n->yy) / d->n->n1;
		d->n->y = (2.0 / 3.0) * d->n->y - 2.0 * d->n->n2 * d->n->y/ d->n->n1;
		d->n->i++;
	}
	if (d->n->x > 0.0)
		render_set_color(25, 125, 225);
	else
	{
		if (d->n->x < -0.3 && d->n->y > 0.0)
			render_set_color(255, 125,  25);
		else
			render_set_color(125, 255, 125);
	}
}

void	run_newton(t_data *d)
{
	w_log("Enter to run_newton");
	d->n->index_y = 0;
	while (d->n->index_y < d->winy)
	{
		d->n->index_x = 0;
		while (d->n->index_x < d->winx)
		{
			run_newton_1(d);
	  		mlx_pixel_image(d->n->index_x, d->n->index_y, d);
			d->n->index_x++;
		}
		d->n->index_y++;
	}
	mlx_put_image_to_window(d->render->mlx,
		d->render->win, d->img_new, 0, 0);
}
