#include "../includes/projet.h"

int	main(int ac, char **av)
{
	static t_data	*d = NULL;

	if (ac < 2)
	{
		print_commands();
		return (0);
	}
	if (init_start(&d, ac, av) < 0)
		return (error("main:: Initialization error"));
	if (check_option(d, ac, av) < 0 || scenar_alloc(d) < 0)
		return (0);
	if (render_init(d) < 0)
		return (error("main:: Render mlx initialisation error"));
	render_scene(d);
	return(0);
}
