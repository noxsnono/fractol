#include "../includes/projet.h"

int	scenar_alloc(t_data *d)
{
	d->j = (t_julia *)malloc(sizeof(t_julia));
	if (NULL == d->j)
		return (error("scenar_alloc:: julia Malloc Failed"));
	d->m = (t_mandelbrot *)malloc(sizeof(t_mandelbrot));
	if (NULL == d->m)
		return (error("scenar_alloc:: julia Malloc Failed"));
	d->n = (t_newton *)malloc(sizeof(t_newton));
	if (NULL == d->n)
		return (error("scenar_alloc:: julia Malloc Failed"));
	return (0);
}
