#include "../includes/projet.h"

static void	mouse_wheel_up_julia_move_x(t_data *d, int x)
{
	if (d->j_zoom > 0)
		d->j_moveX += ((double)(x - (d->winx / 2.0)) / 1000.0);
	else if (d->j_zoom > 4.0)
		d->j_moveX += ((double)(x - (d->winx / 2.0)) / 10000.0);
	else if (d->j_zoom > 6.0)
		d->j_moveX += ((double)(x - (d->winx / 2.0)) / 100000.0);
	else if (d->j_zoom > 8.0)
		d->j_moveX += ((double)(x - (d->winx / 2.0)) / 1000000.0);
}

static void	mouse_wheel_up_julia_move_y(t_data *d, int y)
{
	if (d->j_zoom > 0)
		d->j_moveY += ((double)(y - (d->winy / 2)) / 1000.0);
	else if (d->j_zoom > 4.0)
		d->j_moveY += ((double)(y - (d->winy / 2)) / 10000.0);
	else if (d->j_zoom > 6.0)
		d->j_moveY += ((double)(y - (d->winy / 2)) / 100000.0);
	else if (d->j_zoom > 8.0)
		d->j_moveY += ((double)(y - (d->winy / 2)) / 1000000.0);
}

int		mouse_wheel_up(t_data *d, int x, int y)
{
	if (MANDELBROT == d->scenar)
	{
		if (d->m_zoom + 0.25)
		{
			w_log("MANDELBROT zoom + mol haut");
			d->m_zoom += 0.5;
		}
	}
	else if (JULIA == d->scenar)
	{
		w_log("JULIA zoom + mol bas");
		d->j_zoom += 0.5;
		mouse_wheel_up_julia_move_x(d, x);
		mouse_wheel_up_julia_move_y(d, y);
	}
	return (0);
}
