#include "../includes/projet.h"

int	key_up(t_data *d)
{
	if (JULIA == d->scenar)
	{
		w_log("Key LULIA\tUP");
		d->j_moveY += 0.03;
	}
	else if (NEWTON == d->scenar)
	{
		w_log("Key NEWTON\tUP");
		d->n_deltay += 0.0005;
	}
	return (1);
}

int	key_down(t_data *d)
{
	if (JULIA == d->scenar)
	{
		w_log("Key LULIA\tDOWN");
		d->j_moveY -= 0.03;
	}
	else if (NEWTON == d->scenar)
	{
		w_log("Key NEWTON\tDOWN");
		d->n_deltay -= 0.0005;
	}
	return (1);
}

int	key_a(t_data *d)
{
	if (JULIA == d->scenar)
	{
		w_log("Key JULIA\tA");
		d->c_real += 0.0002;
	}
	return (1);
}

int	key_d(t_data *d)
{
	if (JULIA == d->scenar)
	{
		w_log("Key JULIA\tD");
		d->c_real -= 0.0002;
	}
	return (1);
}

int	key_w(t_data *d)
{
	if (JULIA == d->scenar)
	{
		w_log("Key JULIA\tW");
		d->c_imaginary -= 0.0002;
	}
	return (1);
}
