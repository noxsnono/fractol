#include "../includes/projet.h"

int		key_s(t_data *d)
{
	if (JULIA == d->scenar)
	{
		w_log("Key JULIA\tS");
		d->c_imaginary += 0.0002;
	}
	return (1);
}

static void	key_r_julia(t_data *d)
{
	w_log("Key JULIA\tR");
	d->j_zoom = 1;
	d->j_moveX = 0;
	d->j_moveY = 0;
	d->j_iter_max = JITER;
	d->c_real = JREAL;		// -0.7
	d->c_imaginary = JIMAGINARY;	// 0.27015
}

static void	key_r_mandelbrot(t_data *d)
{
	if (d->m_iter_max - 3 >= 1)
	{
		w_log("Key MANDELBROT\tR");
		d->m_zoom = 1.0;
		d->m_iter_max = MITER;
	}
}

static void	key_r_newton(t_data *d)
{
	w_log("Key NEWTON\tR");
	d->n_iter_max = NITER;
	d->n_deltax = (NXEND - NXBEGIN) / d->winx;
	d->n_deltay = (NYEND - NYBEGIN) / d->winy;
}

int		key_r(t_data *d)
{
	if (JULIA == d->scenar)
		key_r_julia(d);
	else if (MANDELBROT == d->scenar)
		key_r_mandelbrot(d);
	else if (NEWTON == d->scenar)
		key_r_newton(d);
	return (1);
}
