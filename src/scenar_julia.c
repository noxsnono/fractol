#include "../includes/projet.h"

static void	run_julia_1(t_data *d)
{
	d->j->new_real = 1.5 * (d->j->x - d->winx / 2)
		/ (0.5 * d->j_zoom * d->winx) + d->j_moveX;
	d->j->new_imaginary = (d->j->y - d->winy / 2)
		/ (0.5 * d->j_zoom * d->winy) + d->j_moveY;
	d->j->i = 0;
	d->j->run = 1;
	while (d->j->i < d->j_iter_max && 1 == d->j->run)
	{
		d->j->old_real = d->j->new_real;
		d->j->old_imaginary = d->j->new_imaginary;
		d->j->new_real = d->j->old_real
			* d->j->old_real - d->j->old_imaginary
			* d->j->old_imaginary + d->c_real;
		d->j->new_imaginary = 2 * d->j->old_real
			* d->j->old_imaginary + d->c_imaginary;
		if ((d->j->new_real * d->j->new_real + d->j->new_imaginary
			* d->j->new_imaginary) > 4)
			d->j->run = 0;
		if (1 == d->j->run)
			d->j->i++;
	}
}

void		run_julia(t_data *d)
{
	w_log("Enter to run_julia");
	d->j->y = 0;
	while ( d->j->y < d->winy)
	{
		d->j->x = 0;
		while (d->j->x < d->winx)
		{
			run_julia_1(d);
			if (1 == d->j->run)
				render_set_color(255, d->j->new_imaginary
					+ 50, d->j->new_real + 25);
			else
				render_set_color(d->j->new_imaginary,
					255, d->j->new_real);
			mlx_pixel_image(d->j->x, d->j->y, d);
			d->j->x++;
		}
		d->j->y++;
	}
	mlx_put_image_to_window(d->render->mlx,
		d->render->win, d->img_new, 0, 0);
}
