#include "../includes/projet.h"

void	op_win(t_data *d, int ac, char **av)
{
	w_log("Enter to op_win");
	d->i_argv++; // + 1 move -win
	d->op_win = 0;
	if (d->i_argv < ac && NULL != av[d->i_argv] && av[d->i_argv][0] == 'x')
	{
		if (ft_strlen(av[d->i_argv]) > 2 && ft_isdigit(av[d->i_argv][1]) == 1)
		{
			d->winx = ft_atoi(&av[d->i_argv][1]);
			d->i_argv++;
			d->op_win = 1;
		}
	}
	if (d->i_argv < ac && NULL != av[d->i_argv] && av[d->i_argv][0] == 'y')
	{
		if (ft_strlen(av[d->i_argv]) > 2 && ft_isdigit(av[d->i_argv][1]) == 1)
		{
			d->winy = ft_atoi(&av[d->i_argv][1]);
			d->i_argv++;
			d->op_win = 2;
		}
	}
}
