#include "../includes/projet.h"

static void	extra_infos_julia(void)
{
	ft_putstr("\tScenar JULIA ::\n");
	ft_putstr("\tKey\tLEFT arrow\tImage move left\n");
	ft_putstr("\tKey\tRIGHT arrow\tImage nove right\n");
	ft_putstr("\tKey\tUP arrow\tImage move up\n");
	ft_putstr("\tKey\tDOWN arrow\tImage move down\n");
	ft_putstr("\tKey\t'a'\t\tIcrement Real Number\n");
	ft_putstr("\tKey\t'd'\t\tDecrement Real Number\n");
	ft_putstr("\tKey\t'w'\t\tDecrement Imaginary Number\n");
	ft_putstr("\tKey\t's'\t\tIncrement Imaginary Number\n");
	ft_putstr("\tMouse\tMotion\t\tInc/Dec Real & Imaginary Numbers\n");
	ft_putstr("\tMouse\tWheel up\tZoom out\n");
	ft_putstr("\tMouse\tWheel down\tZoom in\n\n\n");
}

static void	extra_infos_mandelbrot(void)
{
	ft_putstr("\tScenar MANDELBROT ::\n");
	ft_putstr("\tMouse\tWheel up\tZoom out\n");
	ft_putstr("\tMouse\tWheel down\tZoom in\n\n\n");
}
static void	extra_infos_newton(void)
{
	ft_putstr("\tScenar NEWTON ::\n");
	ft_putstr("\tKey\tLEFT arrow\tIncrement DeltaX\n");
	ft_putstr("\tKey\tRIGHT arrow\tDecrement DeltaX\n");
	ft_putstr("\tKey\tUP arrow\tDecrement DeltaY\n");
	ft_putstr("\tKey\tDOWN arrow\tIncrement DeltaY\n\n");
}

void	extra_infos(t_data *d)
{
	d->show_help = 1;
	ft_putstr("EXTRA INFOS\n\n");
	ft_putstr("\tGENERAL\n");
	ft_putstr("\tKey\tECHAP\t\tExit\n");
	ft_putstr("\tKey\t'l'\t\tActivate/Desactivate Logs\n");
	ft_putstr("\tKey\t'm'\t\tActivate/Desactivate Mouse Motion Capture\n");
	ft_putstr("\tKey\t'r'\t\tReset params and scenar\n");
	ft_putstr("\tKey\t'+'\t\tIncrement Max Iterations\n");
	ft_putstr("\tKey\t'-'\t\tDecrement Max Iterations\n\n");
	extra_infos_julia();	
	extra_infos_mandelbrot();
	extra_infos_newton();
}
