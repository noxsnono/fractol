#include "../includes/projet.h"

void		print_commands(void)
{
	ft_putstr("\nusage:\n\n");
	ft_putstr("\tfractol ?\n");
	ft_putstr("\tfractol [ -log -nomotion [-win xWIDTH yHEIGHT] ] [Scenar number]\n");
	ft_putstr("\nFractol option:\n\n");
	ft_putstr("\t-log\tRegister logs on logs/pushswap.log\n");
	ft_putstr("\t-motion\tActivate mouse motion capture to change Julia\n");
	ft_putstr("\t?\tShow extra information\n");
	ft_putstr("\t1\tJulia scenar\n");
	ft_putstr("\t2\tMandelbrot scenar\n");
	ft_putstr("\t3\tNewton scenar\n\n");
}

static int	check_option_1(t_data *d, int ac, char **av, int *run)
{
	if (0 == ft_strcmp(av[d->i_argv], "-log"))
		op_logs(d);
	else if (0 == ft_strcmp(av[d->i_argv], "-nomotion"))
		op_mmotion(d);
	else if (0 == ft_strcmp(av[d->i_argv], "-win"))
		op_win(d, ac, av);
	else
	{
		if (1 == ft_isdigit(av[d->i_argv][0]))
		{
			d->scenar = ft_atoi(av[d->i_argv]);
			*run = 0;
			if (d->scenar < 1 || d->scenar > MAXSCENAR)
				return (error("check_option_1:: Bad scenario"));
		}
		else
		{
			return (error("Wrong arguments"));
			d->i_argv++;
		}
	}
	return (0);
}

int		check_option(t_data *d, int ac, char **av)
{
	int	run;

	run = 1;
	d->i_argv = 1;
	if (2 == ac && 0 == ft_strcmp(av[1], "?"))
	{
		extra_infos(d);
		return (-1);
	}
	while (1 == run && d->i_argv < ac)
	{
		if (check_option_1(d, ac, av, &run) < 0)
			return (error("check_option:: Error on parsing args"));
	}
	if (2 == d->op_win && (d->winx < 50|| d->winx > 1920
		|| d->winy < 50 || d->winy > 1080))
		return(error("check_option:: Resolution must between 50x50 to 1920x1080"));
	if (1 == run && 0 == d->show_help)
		return (error("check_option:: Need number for scenar"));
	if (1 == d->op_logs)
		w_log("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nEnter to check_option");
	return (0);
}
