#include "../includes/projet.h"

static void	run_manderlbrot_1(t_data *d)
{
	d->m->cx = ((d->m->index_x / d->winx) - 0.5)
		/ d->m_zoom * 3.0 - 0.7;
	d->m->cy = ((d->m->index_y / d->winy) - 0.5)
		/ d->m_zoom * 3.0;
	d->m->x = 0.0;
	d->m->y = 0.0;
	d->m->i = 0;
	d->m->run = 1;
	while (d->m->i < d->m_iter_max && 1 == d->m->run)
	{
		d->m->xx = d->m->x * d->m->x - d->m->y * d->m->y + d->m->cx;
		d->m->y = 2 * d->m->x * d->m->y + d->m->cy;
		d->m->x = d->m->xx;
		d->m->color_mult = d->m->x * d->m->x + d->m->y * d->m->y;
		if (d->m->color_mult > 100)
			d->m->run = 0;
		d->m->i++;
	}
}

void		run_manderlbrot(t_data *d)
{
	w_log("Enter to run_manderlbrot");
	d->m->index_y = 0;
	while (d->m->index_y < d->winy)
	{
		d->m->index_x = 0;
		while (d->m->index_x < d->winx)
		{
			run_manderlbrot_1(d);
			if (d->m->i == d->m_iter_max)
				render_set_color(255, 50, 50);
			else
				render_set_color((int)(d->m->x * d->m->y * d->m->xx) % 255,
					(int)(d->m->x * d->m->y * d->m->xx) % 255,
					(int)(d->m->x * d->m->y) % 255);
			mlx_pixel_image((int)d->m->index_x, (int)d->m->index_y, d);
			d->m->index_x++;
		}
		d->m->index_y++;
	}
	mlx_put_image_to_window(d->render->mlx,
		d->render->win, d->img_new, 0, 0);
}
