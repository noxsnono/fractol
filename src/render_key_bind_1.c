#include "../includes/projet.h"

int	key_echap(t_data *d)
{
	w_log("Press ECHAP >> Exit Success !!!");
	mlx_destroy_image(d->render->mlx, d->img_new);
	mlx_clear_window(d->render->mlx, d->render->win);
	mlx_destroy_window(d->render->mlx, d->render->win);
	ft_memdel((void **)&d->color);
	ft_memdel((void **)&d->render);
	ft_memdel((void **)&d->j);
	ft_memdel((void **)&d->m);
	ft_memdel((void **)&d->n);
	ft_memdel((void **)&d);
	exit(0);
	return (1);
}

int	key_add(t_data *d)
{
	if (MANDELBROT == d->scenar)
	{
		if (d->m_iter_max + 3 <= 100)
		{
			w_log("Key MANDELBROT itermax + 3");
			d->m_iter_max += 3;
		}
	}
	else if (JULIA == d->scenar)
	{
		if (d->j_iter_max + 2 <= 200)
		{
			w_log("Key JULIA itermax + 2");
			d->j_iter_max += 2;
		}
	}
	else if (NEWTON == d->scenar)
	{
		if (d->n_iter_max + 2 <= 100)
		{
			w_log("Key NEWTON itermax + 2");
			d->n_iter_max +=2;
		}
	}
	return (1);
}

int	key_less(t_data *d)
{
	if (MANDELBROT == d->scenar)
	{
		if (d->m_iter_max - 3 >= 1)
		{
			w_log("Key MANDELBROT itermax - 3");
			d->m_iter_max -= 3;
		}
	}
	else if (JULIA == d->scenar)
	{
		if (d->j_iter_max - 2 >= 1)
		{
			w_log("Key JULIA itermax - 2");
			d->j_iter_max -= 2;
		}
	}
	else if (NEWTON == d->scenar)
	{
		if (d->n_iter_max - 2 >= 0)
		{
			w_log("Key NEWTON itermax - 2");
			d->n_iter_max -=2;
		}
	}
	return (1);
}

int	key_left(t_data *d)
{
	if (JULIA == d->scenar)
	{
		w_log("Key LULIA\tLeft");
		d->j_moveX += 0.03;
	}
	else if (NEWTON == d->scenar)
	{
		w_log("Key NEWTON\tLeft");
		d->n_deltax += 0.0005;
	}
	return (1);
}

int	key_right(t_data *d)
{
	if (JULIA == d->scenar)
	{
		w_log("Key LULIA\tRIGHT");
		d->j_moveX -= 0.03;
	}
	else if (NEWTON == d->scenar)
	{
		w_log("Key NEWTON\tRIGHT");
		d->n_deltax -= 0.0005;
	}
	return (1);
}
