#include "../includes/projet.h"

//mlx_pixel_put ( void *mlx_ptr, void *win_ptr, int x, int y, int color )

void	run_fractol(t_data *d)
{
	if (JULIA == d->scenar)
		run_julia(d);
	else if (MANDELBROT == d->scenar)
		run_manderlbrot(d);
	else if (NEWTON == d->scenar)
		run_newton(d);
}
