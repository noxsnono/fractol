#include "../includes/projet.h"

int	mouse_hook(int button, int x, int y, t_data *d)
{
	unsigned short int 	run;

	w_log("Enter to mouse_hook");
	run = 0;
	if (5 == button) // molette BAS
		run = mouse_wheel_down(d, x, y);
	else if (0 == run && 4 == button) // molette HAUT
		run = mouse_wheel_up(d, x, y);
	run_fractol(d);
	return (0);
}
