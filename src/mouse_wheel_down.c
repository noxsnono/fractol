#include "../includes/projet.h"

static void	mouse_wheel_down_julia_move_x(t_data *d, int x)
{
	if (d->j_zoom > 0)
		d->j_moveX += ((double)(x - (d->winx / 2.0)) / 1000.0);
	else if (d->j_zoom > 4.0)
		d->j_moveX += ((double)(x - (d->winx / 2.0)) / 10000.0);
	else if (d->j_zoom > 6.0)
		d->j_moveX += ((double)(x - (d->winx / 2.0)) / 100000.0);
	else if (d->j_zoom > 8.0)
		d->j_moveX += ((double)(x - (d->winx / 2.0)) / 1000000.0);
}

static void	mouse_wheel_down_julia_move_y(t_data *d, int y)
{
	if (d->j_zoom > 0)
		d->j_moveY += ((double)(y - (d->winy / 2)) / 1000.0);
	else if (d->j_zoom > 4.0)
		d->j_moveY += ((double)(y - (d->winy / 2)) / 10000.0);
	else if (d->j_zoom > 6.0)
		d->j_moveY += ((double)(y - (d->winy / 2)) / 100000.0);
	else if (d->j_zoom > 8.0)
		d->j_moveY += ((double)(y - (d->winy / 2)) / 1000000.0);
}

int		mouse_wheel_down(t_data *d, int x, int y)
{
	if (MANDELBROT == d->scenar)
	{
		if (d->m_zoom - 0.25 > (double)1.0)
		{
			w_log("MANDELBROT zoom - mol bas");
			d->m_zoom -= 0.5;
		}
	}
	else if (JULIA == d->scenar)
	{
		if (d->j_zoom - 0.5 > 0.3)
		{
			w_log("JULIA zoom - mol bas");
			d->j_zoom -= 0.5;
			mouse_wheel_down_julia_move_x(d, x);
			mouse_wheel_down_julia_move_y(d, y);
		}
	}
	return (0);
}
