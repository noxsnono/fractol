#include "../includes/projet.h"

int	render_init(t_data *d)
{
	w_log("Enter to render_init");
	d->render = (t_conf *)malloc(sizeof(t_conf));
	if (NULL == d->render)
		return(error("render_init:: t_conf render Malloc Failed"));
	d->render->mlx = mlx_init();
	if (NULL == d->render->mlx)
		return(error("render_init:: render mlx Failed init"));
	d->render->win = mlx_new_window(d->render->mlx, d->winx, d->winy, TITLE);
	if (NULL == d->render->win)
		return(error("render_init:: render windows Failed init"));
	d->img_new = mlx_new_image(d->render->mlx, d->winx, d->winy);
	if (NULL == d->img_new)
		return (error("render_init:: Create new image Failded"));
	d->img_data = mlx_get_data_addr(d->img_new, &(d->bit_pixel),
					&(d->line_size), &(d->endian));
	if (NULL == d->img_data)
		return (error("render_init:: get Date image Failed"));
	d->color = (t_color *)malloc(sizeof(t_color));
	if (NULL == d->color)
		return (error("render_init:: t_color Malloc Failed"));
	return (0);
}
