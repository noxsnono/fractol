#include "../includes/projet.h"

static void	key_hook_1(int keycode, t_data *d, int *run)
{
	if (0 == *run && 65361 == keycode) // LEFT
		*run = key_left(d);
	else if (0 == *run && 65363 == keycode) // RIGHT
		*run = key_right(d);
	else if (0 == *run && 65362 == keycode) // UP
		*run = key_up(d);
	else if (0 == *run && 65364 == keycode) // DOWN
		*run = key_down(d);
}

static void	key_hook_2(int keycode, t_data *d, int *run)
{
	if (0 == *run && 97 == keycode) // A LEFT
		*run = key_a(d);
	else if (0 == *run && 100 == keycode) // D RIGHT
		*run = key_d(d);
	else if (0 == *run && 119 == keycode) // W UP
		*run = key_w(d);
	else if (0 == *run && 115 == keycode) // S DOWN
		*run = key_s(d);
}

static void	key_hook_3(int keycode, t_data *d, int *run)
{
	if (65307 == keycode || 113 == keycode) // ECHAP Q
		*run = key_echap(d);
	else if (0 == *run && (45 == keycode || 65451 == keycode)) // =/+   NUM +
		*run = key_add(d);
	else if (0 == *run && (61 == keycode || 65453 == keycode) ) // -/_ NUM -
		*run = key_less(d);
	else if (0 == *run && 114 == keycode) // R RESET
		*run = key_r(d);
	else if (0 == *run && 109 == keycode) // M motion capture option
		*run = key_m(d);
	else if (0 == *run && 108 == keycode) // L Logs option
		*run = key_l(d);
}

static void	key_hook_4(int keycode, t_data *d, int *run)
{
	if (49 == keycode || 65436 == keycode) // 1
		*run = key_1(d);
	else if (0 == *run && (50 == keycode || 65433 == keycode)) // 2
		*run = key_2(d);
	else if (0 == *run && (51 == keycode || 65435 == keycode) ) // 3
		*run = key_3(d);
}

int		key_hook(int keycode, t_data *d)
{
	int	run;

	run = 0;
	w_log("Enter to key_hook");
	if (0 == run)
		key_hook_1(keycode, d, &run);
	if (0 == run)
		key_hook_2(keycode, d, &run);
	if (0 == run)
		key_hook_3(keycode, d, &run);
	if (0 == run)
		key_hook_4(keycode, d, &run);
	if (0 == run)
		w_log(ft_itoa(keycode));
	run_fractol(d);
	return (0);
}
