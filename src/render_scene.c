#include "../includes/projet.h"

//mlx_hook(void *win_ptr, int x_event, int x_mask, int (*funct)(), void *param);
int	render_scene(t_data *d)
{
	
	mlx_key_hook(d->render->win, key_hook, d);
	mlx_mouse_hook(d->render->win, mouse_hook, d);
	mlx_pointer_hook(d->render->win, pointer_hook,d);
	mlx_expose_hook(d->render->win, expose_hook, d);
	mlx_loop(d->render->mlx);
	return (0);
}
