#include "../includes/projet.h"

int	pointer_hook(int x, int y, t_data *d)
{
	if (1 == d->op_mmotion && JULIA == d->scenar)
	{
		w_log("Enter to pointer_hook");
		d->c_real = x * JMOVE + JREAL;
		d->c_imaginary = y * JMOVE + JIMAGINARY;
		run_fractol(d);
	}
	return (0);
}
