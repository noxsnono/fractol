#include "../includes/projet.h"

static void	init_value_mandelbrot(t_data *d)
{
	d->m_zoom = 1.0;
	d->m_iter_max = MITER;
}

static void	init_value_julia(t_data *d)
{
	d->j_zoom = 1;
	d->j_moveX = 0;
	d->j_moveY = 0;
	d->j_iter_max = JITER;
	d->c_real = JREAL;		// -0.7
	d->c_imaginary = JIMAGINARY;	// 0.27015
}

static void	init_value_newton(t_data *d)
{
	d->n_iter_max = NITER;
	d->n_deltax = (NXEND - NXBEGIN) / d->winx;
	d->n_deltay = (NYEND - NYBEGIN) / d->winy;
}

static void	init_value(t_data *d)
{
	d->op_logs = 0;
	d->op_mmotion = 1;
	d->scenar = 0;
	d->img_new = NULL;
	d->img_data = NULL;
	d->render = NULL;
	d->bit_pixel = 0;
	d->line_size = 0;
	d->endian = 0;
	d->show_help = 0;
	d->winx = WINX;
	d->winy = WINY;
	init_value_mandelbrot(d);
	init_value_julia(d);
	init_value_newton(d);
}

int		init_start(t_data **d, int ac, char **av)
{
	(void)ac;
	(void)av;
	*d = getData();
	if (NULL == d)
		return (error("init_start:: t_data init is NULL, malloc failed"));
	if (open_log(*d) < 0)
		return (error("init_start:: Open_log error"));
	init_value(*d);
	return (0);
}
