#include "../includes/projet.h"

int		key_m(t_data *d)
{
	if (1 == d->op_mmotion)
	{
		w_log("Motion Capture Desactivated");
		d->op_mmotion = 0;
	}
	else
	{
		w_log("Motion Capture Activated");
		d->op_mmotion = 1;
	}
	return (1);
}

int		key_l(t_data *d)
{
	if (1 == d->op_logs)
	{
		d->op_logs = 0;
		w_log("Logs Desactivated");
	}
	else
	{
		d->op_logs = 1;
		w_log("Logs Activated");
	}
	return (1);
}

int		key_1(t_data *d)
{
	d->scenar = JULIA;
	return (1);
}

int		key_2(t_data *d)
{
	d->scenar = MANDELBROT;
	return (1);
}

int		key_3(t_data *d)
{
	d->scenar = NEWTON;
	return (1);
}
